import numpy as np
import random
import matplotlib.pyplot as plt

def configureSpins(number_of_spins, direction = "down"):
    grid = np.ones(number_of_spins, dtype =  np.int)
    if direction == "down":
        grid = -grid
    elif direction != "up":
        if grid.ndim == 1:
            for idx_c in range(len(grid)):
                grid[idx_c] = random.choice((-1,1))
        else:
            for idx_r, row in enumerate(grid):
                for idx_c, column in enumerate(row):
                    grid[idx_r][idx_c] = random.choice((-1,1))
    return grid

def calculateTotalEnergy(spin_grid):
    total_energy = 0
    if spin_grid.ndim == 1:
        for spin in range(len(spin_grid) - 1):
            total_energy -= spin_grid[spin]*spin_grid[spin + 1]
    else:
        for row in range(spin_grid.shape[0] - 1):
            for column in spin_grid[row]:
                total_energy -= spin_grid[row][column]*spin_grid[row + 1][column]

        for column in range(spin_grid.shape[1] - 1):
            for row in spin_grid[column]:
                total_energy -= spin_grid[row][column]*spin_grid[row][column + 1]
    return total_energy

def getRandomSpin(number_of_spins, dimensions):
    spins = ()
    for dimension in range(dimensions):
        spins += (random.randint(0, number_of_spins - 1),)
    return spins

# A whole lot of book-keeping due to the boundaries of the grid
def getSpinPositions(random_spin_index, number_of_spins):
    list_spin_positions = (random_spin_index,)
    if random_spin_index[0] == 0:
        new_index = (1,) + random_spin_index[1:]
        list_spin_positions += (new_index,)
    elif random_spin_index[0] == number_of_spins - 1:
        new_index = (number_of_spins - 2,) + random_spin_index[1:]
        list_spin_positions += (new_index,)
    else:
        new_index_1 = (random_spin_index[0] - 1,) + random_spin_index[1:]
        new_index_2 = (random_spin_index[0] + 1,) + random_spin_index[1:]
        list_spin_positions += (new_index_1,)
        list_spin_positions += (new_index_2,)
    if len(random_spin_index) == 2 and random_spin_index[1] == 0:
        new_index = random_spin_index[:1] + (1,)
        list_spin_positions += (new_index,)
    elif len(random_spin_index) == 2 and random_spin_index[1] == number_of_spins - 1:
        new_index = random_spin_index[:1] + (number_of_spins - 2,)
        list_spin_positions += (new_index,)
    elif len(random_spin_index) == 2:
        new_index_1 = random_spin_index[:1] + (random_spin_index[1] - 1,)
        new_index_2 = random_spin_index[:1] + (random_spin_index[1] + 1,)
        list_spin_positions += (new_index_1,)
        list_spin_positions += (new_index_2,)
    return list_spin_positions

def getSpins(spin_grid, random_spin_index):
    spins = ()
    for position in getSpinPositions(random_spin_index, spin_grid.shape[0]):
        spins += (spin_grid[position],)
    return spins

# Be carefull, the first argument is the spin that is to be changed
def computeDeltaEnergy(spins):
    delta_energy = 0
    for spin in range(1, len(spins)):
        delta_energy += 2*spins[0]*spins[spin]
    return delta_energy

def computeQFactor(delta_energy, temperature = 0.2):
    beta = 1/temperature
    return np.exp(-beta*delta_energy)

def monteCarloStep(spin_grid, temperature = 0.2):
    number_of_spins = spin_grid.shape[0]
    random_spin_index = getRandomSpin(number_of_spins, spin_grid.ndim)
    spins = getSpins(spin_grid, random_spin_index)
    delta_energy = computeDeltaEnergy(spins)
    delta_spins = 0
    q_factor = computeQFactor(delta_energy, temperature)
    if q_factor > random.random():
        spin_grid[random_spin_index] *= -1
        delta_spins = 2*spin_grid[random_spin_index]
    else:
        delta_energy = 0
    return {"grid" : spin_grid, "delta_energy" : delta_energy, "delta_spins" : delta_spins}

def monteCarloAlgorithm(grid, temperature = 0.2):
    total_energy = calculateTotalEnergy(grid)
    total_magnetization = np.sum(grid)
    for step in range(grid.size):
        monte_carlo = monteCarloStep(grid, temperature)
        grid = monte_carlo["grid"]
        total_energy +=  monte_carlo["delta_energy"]
        total_magnetization += monte_carlo["delta_spins"]
    return {"grid" : grid, "grid_energy" : total_energy, "grid_magnetization" : total_magnetization}

def monteCarloSampling(grid, total_samples, temperature):
    for step_wait in range(total_samples // 10):
        grid = monteCarloAlgorithm(grid , temperature)["grid"]

    sum_grid_energies = 0
    sum_grid_energies_squared = 0
    sum_grid_magnetizations = 0

    for step_sample in range(total_samples):
        monte_carlo = monteCarloAlgorithm(grid , temperature)
        grid = monte_carlo["grid"]
        sum_grid_energies += monte_carlo["grid_energy"]
        sum_grid_energies_squared += monte_carlo["grid_energy"]**2
        sum_grid_magnetizations += monte_carlo["grid_magnetization"]

    average_energy = (sum_grid_energies/total_samples)
    specific_heat = (1/(temperature**2))*(sum_grid_energies_squared/total_samples - average_energy**2)
    average_spin = (sum_grid_magnetizations/total_samples)
    return {"grid" : grid,\
            "U/N" : average_energy/(grid.size),\
             "C/N" : specific_heat/(grid.size),\
             "M/N" : average_spin/(grid.size)}

def computationalPhysics():
    list_grid_sizes = ((2,2), (100, 100))
    for grid_size in list_grid_sizes:
        list_total_samples = (1000, 10000)
        for total_samples in list_total_samples:
            grid = configureSpins(grid_size)
            for temperature_step in range(1,21):
                temperature = 0.2*temperature_step
                monte_carlo = monteCarloSampling(grid, total_samples, temperature)

                beta = 1/temperature
                U_theory = -np.tanh(beta)
                C_theory = (beta/np.cosh(beta))**2
                U_MC = monte_carlo["U/N"]
                C_MC = monte_carlo["C/N"]
                M_MC = monte_carlo["M/N"]

                out_file = open('final_output_hopefully_2','a')
                out_file.write("N: " + str(grid_size[0]) + '\t' + "N_samples: " + str(total_samples) + '\t' +\
                  "T: " + str(temperature) + '\t' + "B: " + str(beta) + '\t' + "U_MC: " + str(U_MC) + '\t' +\
                  "C_MC: " + str(C_MC) + '\t' + "M_MC: " + str(M_MC) + '\n')
                out_file.close()
                print(grid_size, total_samples, temperature)

computationalPhysics()
