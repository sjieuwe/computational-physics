import numpy as np
import matplotlib.pyplot as plt

with open('Ising_problem') as f:
    lines = f.readlines()
    x = [float(line.split()[11]) for line in lines] # C/N mmc
    y = [float(line.split()[15]) for line in lines] # C/N T
    T = [float(line.split()[5]) for line in lines]
#
# print(x[:20])
# print(y[:20])

plt.figure()
# plt.ylim((-1.0, 0.2))
plt.xlim((0.2, 4.0))
plt.plot(T[:20], x[:20], 'r-', label='C/N mmc N = 10 S = 1000')
plt.plot(T[:20], x[20:40], 'r--', label='C/N mmc N = 10 S = 10000')
plt.plot(T[:20], x[40:60], 'g-', label='C/N mmc N = 100 S = 1000')
plt.plot(T[:20], x[60:80], 'g--', label='C/N mmc N = 100 S = 10000')
plt.plot(T[:20], x[80:100], 'm-', label='C/N mmc N = 1000 S = 1000')
plt.plot(T[:20], x[100:120], 'm--', label='C/N mmc N = 1000 S = 10000')
plt.plot(T[:20], y[:20], c='b', label='C/N theory')
plt.xlabel('Temperature')
plt.ylabel('Specific heat per atom (C/N)')
plt.title('Results MMC simulation for C/N')
plt.legend()
plt.savefig('Ising_MMC_C_1d.png')
plt.show()
