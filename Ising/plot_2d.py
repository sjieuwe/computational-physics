import numpy as np
import matplotlib.pyplot as plt

with open('final_output_hopefully') as f:
    lines = f.readlines()
    x = [float(line.split()[9]) for line in lines] # U/N2
    y = [float(line.split()[11]) for line in lines] # C/N2
    T = [float(line.split()[5]) for line in lines]
    M = [float(line.split()[13]) for line in lines] # M/N2

def magnetizationTheoretical(temperature):
        beta = 1/temperature
        T_c = 2/(np.log(1 + np.sqrt(2)))
        if temperature < T_c:
            return (1 - np.sinh(2*beta)**(-4))**(1/8)
        else:
            return 0

M_theory = np.zeros(20)
for index, temperature in enumerate(T[:20]):
    M_theory[index] = magnetizationTheoretical(temperature)

# print(x[:20])
# print(y[:20])

plt.figure()
plt.ylim((-1.0, 1.5))
plt.xlim((0.2, 4.0))
plt.plot(T[:20], M[:20], 'r-', label='M/(N^2) mmc N = 10 S = 1000')
plt.plot(T[:20], M[20:40], 'r--', label='M/(N^2) mmc N = 10 S = 10000')
plt.plot(T[:20], M[40:60], 'g-', label='M/(N^2) mmc N = 25 S = 1000')
plt.plot(T[:20], M[60:80], 'g--', label='M/(N^2) mmc N = 25 S = 10000')
plt.plot(T[:20], M[80:100], 'm-', label='M/(N^2) mmc N = 100 S = 1000')
plt.plot(T[:20], M[100:120], 'm--', label='M/(N^2) mmc N = 100 S = 10000')
plt.plot(T[:20], M_theory, 'b')
plt.plot(T[:20], -M_theory, 'b', label='M/(N^2) theory')
plt.xlabel('Temperature')
plt.ylabel('Magnetization per atom (M/(N^2))')
plt.title('Results MMC simulation for M/(N^2)')
plt.legend()
plt.savefig('Ising_MMC_M_2d.png')
plt.show()
