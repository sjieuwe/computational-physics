import numpy as np
import matplotlib.pyplot as plt

with open('Ising_problem') as f:
    lines = f.readlines()
    x = [float(line.split()[9]) for line in lines] # U/N mmc
    y = [float(line.split()[13]) for line in lines] # U/N T
    T = [float(line.split()[5]) for line in lines]
#
# print(x[:20])
# print(y[:20])

plt.figure()
plt.ylim((-1.0, 0.2))
plt.xlim((0.2, 4.0))
plt.plot(T[:20], x[:20], 'r-', label='U/N mmc N = 10 S = 1000')
plt.plot(T[:20], x[20:40], 'r--', label='U/N mmc N = 10 S = 10000')
plt.plot(T[:20], x[40:60], 'g-', label='U/N mmc N = 100 S = 1000')
plt.plot(T[:20], x[60:80], 'g--', label='U/N mmc N = 100 S = 10000')
plt.plot(T[:20], x[80:100], 'm-', label='U/N mmc N = 1000 S = 1000')
plt.plot(T[:20], x[100:120], 'm--', label='U/N mmc N = 1000 S = 10000')
plt.plot(T[:20], y[:20], c='b', label='U/N theory')
plt.xlabel('Temperature')
plt.ylabel('Average energy per atom (U/N)')
plt.title('Results MMC simulation for U/N')
plt.legend()
plt.savefig('Ising_MMC_U_1d.png')
plt.show()
