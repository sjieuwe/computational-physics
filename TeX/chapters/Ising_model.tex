\section{The Ising model}

\subsection{Theory}
The Ising model for magnetism describes the statistical mechanics of a ferromagnetic material. The model considers all atoms in the material to be either in a magnetic spin up state or in a spin down state. The model further asumes that each atom will only have a magnetic interaction with its nearest neighbours. In a one dimensional case, where all atoms form a chain, this means that each atom has at most two atoms with which it will interact. In a two dimensional sample, where the atoms are in a grid, this number will be at most four.\\
Since each spin can be either up or down, there are many ways in which a chain or grid can be formed. From statistical mechanics we can deduce the probability that the system is in a particular configuration:
\begin{align}\label{P_conf}
  P(\text{configuration}) &= \frac{e^{-E/T}}{\sum\limits_{\{S_{1},...,S_{N}\}}{e^{-E/T}}},
\end{align}
where the summation is taken over all possible configurations(, and $k_{B} = 1$ for convenience). From this can be seen that the probability of having a high energy configuration is lower and also that higher temperatures increase the probability of all configurations.\\
In order to calculate the probabities of each configuration, the energy is needed. For a one dimensional Ising model this means that the energy of a specific configuration of spins is given by:
\begin{align}\label{E_1d}
  E &= - \sum_{n = 1}^{N-1}{S_{n}S_{n+1}},
\end{align}
where $S_{n}$ denotes the spin state of the nth atom in the chain of $N$ atoms. From this can be seen that indeed the spins only interact with their neighbours. Similarly, the energy of a two dimensional grid of spins is given by:
\begin{align}\label{E_2d}
  E &= - \sum_{i = 1}^{N - 1}{\sum_{j = 1}^{N}{S_{i,j}S_{i+1,j}}} - \sum_{i = 1}^{N}{\sum_{j = 1}^{N-1}{S_{i,j}S_{i,j+1}}}.
\end{align}
While this equation seems a lot more involved, it is still just the sum op the products of neighbouring spins.\\
Once the energy is known this can be used to compute the average energy $U$, which is given by:
\begin{align}\label{U}
  U &= \frac{\sum\limits_{\{S_{1},...,S_{N}\}}{Ee^{-\beta E}}}{\sum\limits_{\{S_{1},...,S_{N}\}}{e^{-\beta E}}},
\end{align}
the specific heat $C$:
\begin{align}\label{C}
  C &= \frac{\sum\limits_{\{S_{1},...,S_{N}\}}{E^{2}e^{-\beta E}}}{\sum\limits_{\{S_{1},...,S_{N}\}}{e^{-\beta E}}} - U^{2},
\end{align}
and the average magnetization per spin $M$:
\begin{align}\label{M}
  M &= \frac{\sum\limits_{\{S_{1,1},...,S_{N,N}\}}\sum\limits_{i,j=1}^{N-1}{S_{i,j}e^{-\beta E}}}{\sum\limits_{\{S_{1,1},...,S_{N,N}\}}{e^{-\beta E}}},
\end{align}
where $\beta = 1/T$.
In the one dimensional case there is a analytical solution for $U$ and $C$, which are given by:
\begin{align}\label{UC_ana}
  U/(N - 1) &= -\tanh{\beta},\\
  C/(N - 1) &= (\beta/\cosh{\beta})^{2}.
\end{align}
These analytical solutions will provide a good reference point for a numerical solution of the Ising model.\\

\subsection{Numerical model}
In order to solve the Ising model, it is useful to use a numerical approach, especially for higher dimension versions of the problem, where a suitable analytical solution does not exist yet. In order to solve this statistical mechanical problem numerically, it is useful to consider a Metropolis Monte Carlo method. Simply put, in our case the MMC-method will generate random spin configurations while still considering the probabities of each configuration. By averaging all the randomly generated configurations it is then possible to calculate statistical properties like the average energy $U$, specific heat $C$ and the average magnetization per spin $M$.\\
The basis of the MMC-method follows a series of steps:
\begin{itemize}
  \item Pick a spin configuration $\{S_{1},...,S_{N}\}$
  \item Calculate the energy of this configuration
  \item Loop through the following, $N$ times:
  \begin{itemize}
    \item Pick a random spin
    \item Calculate the energy difference $(dE)$ if you would flip the spin
    \item Compute $q = P(E + dE)/P(E) = e^{-\beta dE}$
    \item If $q > r$, accept the flip. ($r$ is a random number)
  \end{itemize}
\end{itemize}
While this algorithm may look a bit strange at first, there are a few things that immediately become clear. Whether a spin flips or not highly depends on the value of $q$, which in turn depends on the change in energy. This means that when a spin flip would yield a lower energy for the system, it is more likely to occur. For the Ising model, the energy is lowest when all spins are pointing in the same direction, as a result the spins that are most likely to flip are those that have all neighbours pointing in the other direction.
There is however still a small chance that a spin flips even though it is not favorable, due to the randomness of $r$. Furthermore, there is a term $\beta$ appearing in $q$, which reduces the effect of the energy difference $dE$ for higher temperatures, which means that spins will flip more easily if the temperature of the system is higher. As an example, take a look at figures \ref{fig:MMC_T04} and \ref{fig:MMC_T20}, in each figure the state of the spin configuration is shown after doing the MMC-algorithm 10 times. Purple means a particular spin is up, and yellow means it's down. In both grids it can be seen that spins tend to have the same orientations as their neighbours, however at the higher temperature there are more spins that are in an energetically unfavorable state.
\begin{figure}
	\includegraphics[width=\columnwidth]{../Ising/images/MMC_Ising_2d_T=04.png}
	\caption{A 10 by 10 spin grid at $T = 0.4$ after doing the MMC-algorithm 10 times. Domains have formed}
  \label{fig:MMC_T04}
\end{figure}

\begin{figure}
	\includegraphics[width=\columnwidth]{../Ising/images/MMC_Ising_2d_T=20.png}
  \caption{A 10 by 10 spin grid at $T = 2.0$ after doing the MMC-algorithm 10 times. Domains have formed, but there is more randomness in the system. For a nice comparison look at "MMC\_Ising\_2d.mp4"}
  \label{fig:MMC_T20}
\end{figure}
A few last remarks about the MMC-algorithm. The loop does as many steps as there are spins, which means that there is a chance that each spins has a chance to be flipped, this ensures that the algorithm is viable for any size of configuration. Furthermore, due to the nature of the $q$-factor, the MMC-algorithm in a sense searches randomly for the most probable/important spin configurations.

In order to determine statistical mechanical quantities, the idea is to generate many random configurations of spins using the MMC-algorithm and somehow take the average of those configurations. This can be done by using the folling general formula:
\begin{align}\label{AVG}
    F &= \frac{\sum\limits_{\{S_{1},...,S_{N}\}}{f(S_{1},...,S_{N})e^{-\beta E}}}{\sum\limits_{\{S_{1},...,S_{N}\}}{e^{-\beta E}}} \\
      &= \frac{1}{\#\Omega}\sum\limits_{\{S_{1},...,S_{N}\}}{f(S_{1},...,S_{N})},
\end{align}
Where $\Omega$ is the number of configurations that have been summed.

Using this formula, $C$, $U$ and $M$, can be calculated by taking so-called Monte Carlo samples. This is done by using the MMC-algorithm  $N_{wait}$ times to let the system of spins relax to the most probable states. Afterwards the MMC-algorithm is called $N_{sample}$ times in order to do a meassurement of the system. This yields $N_{sample}$ samples and will allow the calculation of the physical quantities needed.

\subsection{Results}
The MMC simulation has been run for both the one dimensional Ising model and the two dimensional version. In the one dimensional case, the calculated $U$ and $C$ have been compared to the theoretical value. In the two dimensional case this was done for the net magnetization of the sample.
  \subsubsection{1-dimensional model}
For the one dimensional model chains of $10, 100,$ and  $1000$ atoms respectively have been simulated using $1000$ and $10000$ MMC samples over a temperature range of $0.2$ up till $4.0$.\\
The results of the simulation are shown in the following graphs, should specific values be needed then they can be found in the appendix.
First take a look at the average energy per atom:
\begin{figure}
	\includegraphics[width=\columnwidth]{../Ising/images/Ising_MMC_U_1d.png}
  \caption{Plots of the average energy per atom. The blue line represents the theoretical values, the others are simulated.}
  \label{fig:U_1d}
\end{figure}

Figure \ref{fig:U_1d} clearly shows a good agreement for chains that are longer than $10$ atoms.

Similarly, figure \ref{fig:C_1d} shows the specific heat per atom as a function of temperature. From figure \ref{fig:C_1d} a good agreement between simulation and theory can be seen, however this hold especially for the simulations that took more samples.

\begin{figure}
	\includegraphics[width=\columnwidth]{../Ising/images/Ising_MMC_C_1d.png}
  \caption{Plots of the specific heat per atom. The blue line represents the theoretical values, the others are simulated.}
  \label{fig:C_1d}
\end{figure}

\subsubsection{2-dimensional model}
Just as with the one dimensional model, this section will present the plots of the outcomes of the simulations, the simulated values can be found in the appendix. In the two dimensional case there will be no theoretical values of both $U/N^{2}$ and $C/N^{2}$, however there will be an average magnetization per atom, which will be called $M/N^{2}$, which is defined on page \pageref{M}.

Just as with the one dimensional model, start with the average energy per atom:
\begin{figure}
	\includegraphics[width=\columnwidth]{../Ising/images/Ising_MMC_U_2d.png}
  \caption{Plots of the average energy per atom, all simulated}
  \label{fig:U_2d}
\end{figure}
Even though there is no theoretical prediction for the average energy per atom figure \ref{fig:U_2d} shows the same kind of curve as its one dimensional counterpart in figure \ref{fig:U_1d}, but with more curvature.

\begin{figure}
	\includegraphics[width=\columnwidth]{../Ising/images/Ising_MMC_C_2d.png}
  \caption{Plots of the specific heat per atom, all simulated.}
  \label{fig:C_2d}
\end{figure}

Figure \ref{fig:C_2d} is where things become interesting. This is where different grid sizes yield different results, even though all physical quantities have been normalized for the amount of atoms. To understand this phenomenon it is a good idea to look at the magnetization, given in figure \ref{fig:M_2d}. In this figure it can be clearly seen that for small grid sizes the simulation can diverge quite some bit from the theoretical values, however at higher grid sizes the agreement becomes evident. The lack of the positive average magnetization from the simulations is due to the fact that the simulations all started with all spins pointing down, this is however completely equivalent to the all spins up situation and hence was left out.

\begin{figure}
	\includegraphics[width=\columnwidth]{../Ising/images/Ising_MMC_M_2d.png}
  \caption{Plots of the average magnetization per atom, the blue line represents the theoretical values.}
  \label{fig:M_2d}
\end{figure}


\subsection{Conclusions}
The results presented in the previous paragraph can be best interpreted in a systematic way, starting with the one dimensional case.

\subsubsection{1-dimensional model}
From the comparison between the simulated values of $C/N$ and $U/N$ with their theoretical counterpart one can deduce that the MMC simulation is indeed yielding proper results. The agreement between the values is especially for longer chains exceptional, which is expected due to the continuum nature of the theoretical result. Since the simulation inherently relies on the random nature of (pseudo-)random numbers, it makes sense that one atom can be in a different state than the theoretical model would predict, which for smaller grid sizes has a much bigger impact than for bigger ones.
The most important result from the one dimensional model however is that there is an agreement between the simulation and the theory and also that is is quite big.

\subsubsection{2-dimensional model}
The two dimensional model is much more interesting than the one dimensional one in terms of physics and in terms of results. Figures \ref{fig:MMC_T04}
and \ref{fig:MMC_T20} show that after sufficiently many MMC steps there are already interesting things to see. The spins seems to have formed domains within the grid, either pointing up or pointing down. Unless the domains have the same size, this would yield a net magnetization.
This magnetization turns out to be quite important in the explanation of the odd behaviour of the specific heat. Taking a look at the average magnetization per atom in figure \ref{fig:M_2d}, show that at a critical temperature it disappears, this corresponds to a phase change in the material. From the figure one can deduce that the phase change occurs around a temperature of $2.2$.
This value returns in figure \ref{fig:C_2d}, which shows the specific heat per atom. Oddly enough the value of the specific heat around the critical temperature seems to grow with grid size, which would mean that in a continuum approximation of the problem the specific would tend to infinity.
This indeed corresponds with other descriptions of second order phase changes, where the first derivative of the free energy is continuous but the second derivative is infinite.
Since the specific heat is the derivative of the average energy with respect to temperature, this would mean that around the critical temperature the slope of the average energy tends to infinity for the continuum approximation. Taking a look at figure \ref{fig:U_2d} shows indeed that the slope increases with the grid size, which is in agreement with current models of second order phase changes.
