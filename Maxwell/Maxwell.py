import numpy as np
import matplotlib.pyplot as plt

# Parameters used in the problem
# Physical parameters
wavelength = 1
index_of_refraction = 1.31
length_box = 100*wavelength

def current_source(time):
    return np.sin(2*np.pi*time)*np.exp(-((time-30)/10)**2)

# Material parameters
mu = 1

def sigma(pos_x):
    if pos_x <= 6*wavelength or (pos_x >= length_box - 6*wavelength):
        return 1
    else:
        return 0

def epsilon(pos_x):
    if pos_x < length_box/2: #or (pos_x >= length_box/2 + 2*wavelength and pos_x <= length_box):
        return 1
    else:
        return index_of_refraction**2

# Computational parameters
x_step_size = wavelength/50
t_step_size = 0.9*x_step_size # due to the Courant condition
total_x_steps = int(length_box/x_step_size)
total_t_steps = 2*total_x_steps

# Implementation of the FDTD simulation
# terms with mu are left out (mu = 1) in the following functions
def updateMagneticField(magnetic_field, electric_field, x_step):
    delta_magn_field = (electric_field[x_step + 1] - electric_field[x_step])/x_step_size -\
                        sigma(x_step_size*x_step)*magnetic_field[x_step]
    return delta_magn_field

def updateElectricField(magnetic_field, electric_field, x_step, time):
    delta_el_field = 1/(epsilon(x_step_size*x_step))*(magnetic_field[x_step] - magnetic_field[x_step - 1])/x_step_size -\
                    (sigma(x_step_size*x_step))/(epsilon(x_step_size*x_step))*electric_field[x_step]
    if x_step == total_x_steps/5:
        delta_el_field -= current_source(time)
    return delta_el_field

def FDTDSimulation():
    magnetic_field = np.zeros(total_x_steps)
    electric_field = np.zeros(total_x_steps + 1)

    max_ref = 0
    min_ref = 0
    max_inc = 0
    min_inc = 0

    for t_step in range(total_t_steps):
        for x_step in range(total_x_steps):
            magnetic_field[x_step] += t_step_size*updateMagneticField(magnetic_field, electric_field, x_step)
        for x_step in range(total_x_steps):
            electric_field[x_step] += t_step_size*updateElectricField(magnetic_field, electric_field, x_step, t_step_size*t_step)


        electric_field[0] = 0
        electric_field[5000] = 0

        print(t_step)
        # list_times = (500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500)
        # if t_step in list_times:
        #     plt.figure()
        #     plt.plot(electric_field)
        #     plt.axis([0, 5000, -0.02, 0.02])
        #     plt.ylabel("Electic field")
        #     plt.xlabel("Grid point")
        #     plt.title("Propagation of light through a medium at t=" + str(t_step))
        #     plt.axvspan(0, 300, facecolor='0.2', alpha=0.5)
        #     plt.axvspan(4700, 5000, facecolor='0.2', alpha=0.5)
        #     plt.axvspan(2500, 5000, facecolor='g', alpha=0.5)
        #     plt.savefig("maxwell_at_time=" + str(t_step) + ".png")
        #     plt.close()

        if t_step > total_t_steps//2:
            max_r = np.amax(electric_field[:total_x_steps//2])
            min_r = np.amin(electric_field[:total_x_steps//2])
            if  max_r >= max_ref:
                max_ref = max_r
            if min_r < min_ref:
                min_ref = min_r


        max_i = np.amax(electric_field[total_x_steps//2:])
        min_i = np.amin(electric_field[total_x_steps//2:])
        if  max_i >= max_inc:
            max_inc = max_i
        if min_i < min_inc:
            min_inc = min_i

    print(max_ref, max_inc)
    print(min_ref, min_inc)

    max_ref = max(max_ref, abs(min_ref))
    max_inc = max(max_inc, abs(min_inc))

    print(max_ref**2/max_inc**2)

FDTDSimulation()
