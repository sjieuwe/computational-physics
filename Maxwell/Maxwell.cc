#include <vector>
#include <cmath>
#include <iostream>
#include <algorithm>

#define PI 3.14159265358979323846264338327950288419716939937510

int const wavelength = 1;
int const length_box = 100;
double const index_of_refraction = 1.31;

double const current_source(double const &time){
  return sin(2*PI*time)*exp(-(pow((time - 30)/10, 2)));
}

int const sigma(double const &pos_x)
{
  if (pos_x <= 6*wavelength or (pos_x >= length_box - 6*wavelength)){
    return 1;
  } else {
    return 0;
  }
}

double const epsilon(double const &pos_x){
  return (pos_x <= length_box/2 or (pos_x >= length_box/2 + 2)) ? 1.0 : pow(index_of_refraction, 2);
}
// Computational parameters
int const inverse_x_step_size = 50;
double const x_step_size = 1/((double)inverse_x_step_size);
double const t_step_size = 0.9*x_step_size; // due to the Courant condition
int const total_x_steps = 100*inverse_x_step_size;
int const total_t_steps = 2*total_x_steps;

// Implementation of the FDTD simulation
// terms with mu are left out (mu = 1) in the following functions
double const updateMagneticField(std::vector<double> const &magnetic_field,
                                 std::vector<double> const &electric_field,
                                 int const &x_step){
    double const delta_magn_field = (electric_field[x_step + 1] - electric_field[x_step])*inverse_x_step_size
                                  -sigma(x_step_size*(x_step + 0.5))*magnetic_field[x_step];
    return delta_magn_field;
}

double updateElectricField(std::vector<double> const &magnetic_field,
                                 std::vector<double> const &electric_field,
                                 int const &x_step,
                                 double const &time){
    double delta_el_field = 1/(epsilon(x_step_size*x_step))*(magnetic_field[x_step] - magnetic_field[x_step - 1])*inverse_x_step_size
                          -(sigma(x_step_size*x_step))/(epsilon(x_step_size*x_step))*electric_field[x_step];
    if (x_step == total_x_steps/5){
        delta_el_field -= current_source(time);
    }
    return delta_el_field;
}

int main()
{
  std::vector<double> magnetic_field(total_x_steps);
  std::vector<double> electric_field(total_x_steps + 1);

  double true_max_inc = 0.0;
  double true_max_ref = 0.0;

  for (std::size_t t_step = 0; t_step != total_t_steps; ++t_step){
    for (std::size_t x_step = 0; x_step != total_x_steps; ++x_step){
      magnetic_field[x_step] += t_step_size*updateMagneticField(magnetic_field, electric_field, x_step);
    }
    for (std::size_t x_step = 0; x_step != total_x_steps; ++x_step){
      electric_field[x_step] += t_step_size*updateElectricField(magnetic_field, electric_field, x_step, t_step_size*t_step);
    }
    electric_field[0] = 0.0;
    electric_field[total_x_steps] = 0.0;

    if (t_step <= total_t_steps/4){
      double max_inc = *std::max_element(electric_field.begin(), electric_field.begin() + total_x_steps/2);
      double min_inc = *std::min_element(electric_field.begin(), electric_field.begin() + total_x_steps/2);
      true_max_inc = std::max(true_max_inc, std::max(max_inc, std::abs(min_inc)));
    }

    if (t_step >= total_t_steps/2){
      double max_ref = *std::max_element(electric_field.begin(), electric_field.begin() + total_x_steps/2);
      double min_ref = *std::min_element(electric_field.begin(), electric_field.begin() + total_x_steps/2);
      true_max_ref = std::max(true_max_ref, std::max(max_ref, std::abs(min_ref)));
    }


    // std::cout << t_step << '\n';
  }

  std::cout << true_max_inc << '\t' << true_max_ref << '\t' << pow(true_max_ref, 2)/pow(true_max_inc, 2) << '\n';
}
